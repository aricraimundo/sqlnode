const { Op, json } = require("sequelize");

const Address = require("../models/Address");
const User = require("../models/User");
const Tech = require("../models/Tech");

module.exports = {
  async show(req, res) {
    const users = await User.findAll({
      attributes: ["name", "email"],
      where: {
        email: {
          [Op.iLike]: "%hotmail.com",
        },
      },
      include: [
        {
          association: "addresses",
          where: {
            street1: {
              [Op.iLike]: "%Parintins%",
            },
          },
        },
        {
          association: "techs",
          required: false, // OUTER JOIN (not mandatory - does not return anything if doesn't exist)
          where: {
            name: {
              [Op.iLike]: "C%",
            },
          },
        },
      ],
    });

    return res.json(users);
  },
};
